'use strict';

const gulp = require ('gulp');
const pug = require ('gulp-pug');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
sass.compiler = require('node-sass');
const browserSync = require ('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require ('gulp-sourcemaps');
const gulpIf = require ('gulp-if');
const del = require ('del');
const cleanCSS = require ('gulp-clean-css');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const uglify = require('gulp-uglify');
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

gulp.task('buildHTML', ()=>{
  return gulp.src('src/*.pug')
                .pipe (gulpIf(isDevelopment, sourcemaps.init()))
                .pipe (pug({
                    pretty: true
                  }))
                .pipe (gulpIf(isDevelopment, sourcemaps.write()))
                .pipe (gulp.dest('public'))

});

gulp.task('buildSass', function () {
  return gulp.src(['src/styles/*.scss', '!src/styles/_*.scss', '!src/styles/**/_*.scss'])
    .pipe(sass({
      includePaths: require('node-normalize-scss').includePaths,
      importer: require('npm-sass').importer
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('public'));
});

gulp.task('sassMin', function () {
  return gulp.src(['src/styles/*.scss', '!src/styles/_*.scss', '!src/styles/**/_*.scss'])
    .pipe(sass({
      includePaths: require('node-normalize-scss').includePaths,
      importer: require('npm-sass').importer
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cleanCSS({
      level: 2}))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('public'));
});

gulp.task('buildJS', ()=>{
  return browserify({entries: ['src/js/index.js'], debug: true})
    .transform("babelify", { presets: ["@babel/preset-env"] })
    .bundle()
    .pipe(source('index.js'))
    .pipe(gulp.dest('public'))
    .pipe(browserSync.stream());
});

gulp.task('jsMin', ()=>{
  return gulp.src('src/js/index.js')
    .pipe(babel({
      presets: ["@babel/preset-env"],
    }))
    .pipe(uglify())
    .pipe(gulp.dest('public'))
});

gulp.task('buildAssets', ()=> {
  return gulp.src('src/assets/**/**/*.*')
                .pipe(gulp.dest('public/assets'))
});

gulp.task('favicon', ()=> {
  return gulp.src('./src/*.png')
    .pipe(gulp.dest('public/'))
});

gulp.task('clean', ()=>{
  return del('public');
});

gulp.task('buildLocal', gulp.series('clean', gulp.parallel('buildHTML', 'buildSass', 'buildAssets', 'favicon', 'buildJS')));

gulp.task('watch', ()=>{
  gulp.watch('./src/js/**/*.js', gulp.series('buildJS'));
  gulp.watch('./src/styles/**/*.scss', gulp.series('buildSass'));
  gulp.watch('./src/assets/**/**/*.*', gulp.series('buildAssets'));
  gulp.watch('./src/**/**/*.pug', gulp.series('buildHTML'))
});

gulp.task('serve', () => {
  browserSync.init({
    server: "public"
  });
  browserSync.watch('public/**/**/*.*').on('change', browserSync.reload);
});

gulp.task('local',
  gulp.series('buildLocal', gulp.parallel('watch', 'serve'))
);

gulp.task('prod', gulp.series('clean', gulp.parallel('buildHTML', 'buildAssets', 'sassMin', 'favicon', 'jsMin')));


