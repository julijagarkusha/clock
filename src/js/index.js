const root = document.querySelector(':root');

const startTime = () => {
  const today = new Date();
  const hours = today.getHours();
  const minutes = today.getMinutes();
  const seconds = today.getSeconds();

  root.style.setProperty('--start-hours', `${hours*30}deg`);
  root.style.setProperty('--start-minutes', `${minutes*6}deg`);
  root.style.setProperty('--start-second', `${seconds*6}deg`);
  document.querySelector('.clock').classList.add('clock--animate');
};

startTime();

